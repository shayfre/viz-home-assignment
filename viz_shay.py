import os, sys, tarfile
import urllib.request
import pydicom
import shutil
from datetime import timedelta
from datetime import datetime

class Patient:
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def print_patient(self):
        print("name: " + self.name + " age:" + self.age + " sex:" + self.sex)

def strToTimeDelta(time):
    if len(time)==6:
        dt = datetime.strptime(time, '%H%M%S')        
    elif len(time)==13:
        dt = datetime.strptime(time, '%H%M%S.%f')        
    else:
        print("Invalid series time format: " + time)
    delta = timedelta(hours=dt.hour,minutes=dt.minute,seconds=dt.second,microseconds=dt.microsecond)
    return delta

def dicoms_handler(url):
    tar_name = os.path.basename(url)
    urllib.request.urlretrieve(url, tar_name)
    tar = tarfile.open(tar_name, 'r')
    
    for item in tar:
        tar.extract(item)
    
    dicoms = tar.getnames()
    
    patients = []
    series_times = {} #key:series id, value:[start_time, end_time]
    hospitals = set()
    
    for dcm in dicoms:
        ds = pydicom.dcmread(dcm)    
        name = str(ds.PatientName)    
        study = str(ds.StudyInstanceUID)
        series = str(ds.SeriesInstanceUID)    
        dst = name+'/'+study+'/'+series
            
        if os.path.isdir(name) == False:        
            age = ds.PatientAge
            sex = ds.PatientSex
            patient = Patient(name, age, sex)
            patients.insert(len(patients), patient)
            
        if os.path.isdir(dst) == False:
            os.makedirs(dst)
        shutil.move(dcm, dst) # arranges the files according to the DICOM hierarchy
        
        # get series duration
        series_start = strToTimeDelta(ds.SeriesTime) # time the Series started.
        inst_time = strToTimeDelta(ds.ContentTime) # time this instance creation started        
        if series not in series_times:
            series_times[series] = [series_start, inst_time]
        if ((series_times[series])[1]) < inst_time: 
            (series_times[series])[1] = inst_time # save the latest instance of this series
        
        hospital = ds.InstitutionName
        if hospital not in hospitals:
            hospitals.add(hospital)
    
    # calc average ct duration
    ct_durations_sum = timedelta()  # init to 0   
    for series in series_times.values():           
        start_time = series[0]
        end_time = series[1]
        if end_time < start_time: # in case scan was around midnight, add 24 hours
            end_time += timedelta(hours=24)
        series_duration = end_time - start_time
        ct_durations_sum += series_duration
    ct_average_duration = ct_durations_sum/len(series_times)
    
    return (patients, hospitals, ct_average_duration)

def print_results(patients, hospitals, ct_average_duration):
    print("There are " + str(len(patients)) + " patients")    
    for p in patients:
        p.print_patient()
        
    print("The data came from " + str(len(hospitals)) + " different hospitals")
       
    print("Average time for CT scan is: " + str(ct_average_duration))

def main():
    if len(sys.argv) != 2:
        print("Please supply URL\n")
        sys.exit()
    
    url = sys.argv[1]  # https://s3.amazonaws.com/viz_data/DM_TH.tgz    
        
    (patients, hospitals, ct_average_duration) = dicoms_handler(url)
    print_results(patients, hospitals, ct_average_duration)
    
    
if __name__ == "__main__":
    main()
    

"""
Average duration of CT scan calculation:

Series is a single scan and it contains number of instances (images).
The scan duration is not available directly since the tag “(0018,0072) Effective Duration” does not exist in any of the provided DICOM files.
The scan duration can be calculated as the time between series start time and series end time. Series start time is given as “(0008,0031) Series Time” tag.
Approximate series end time can be the “Content Time” of the last instance of the series. 
Please note that this calculation does not consider the creation duration of the last instance in the series. 
Since the creation duration of a single instance is very short it may be negligible.

Notes:
-	Exploring the DICOM tags, I found no tag that refer to the series end time or instance end time.
-	(0008,0013) InstanceCreationTime exists in only part of the provided DICOM files, and that is the reason I used (0008,0033) ContentTime instead. For files that contain both tags, the values are equal.




"""